from django import template
import locale

register = template.Library()
locale.setlocale(locale.LC_ALL, '')


def currency(cents):
    return locale.currency(cents/100, grouping=True) if isinstance(cents, int) else ''


register.filter('currency', currency)
