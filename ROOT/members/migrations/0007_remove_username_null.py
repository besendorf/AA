# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import members.models


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0006_populate_unique_usernames'),
    ]

    operations = [
        migrations.AlterField(
            model_name='member',
            name='username',
            field=models.CharField(default=members.models._unique_username, max_length=50, null=False, unique=True),
        ),

    ]
