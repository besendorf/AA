function home() {
    var con = $('#mainContainer');
    con.empty();
    con.append("Test");
}


function getPendingMails() {
    jQuery.ajax({
        type: 'GET',
        url: '/api/msg_stats',
        dataType: 'json',
        success: function(data) {
            for (field in data) {
                $('#' + field + 'FieldAnchor').text(data[field]);
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert("ERROR in message stats: '" + textStatus + "' error thrown: '" + errorThrown + "'");
        }
    });
}


function contentControl() {
    var id = $.find(".active")[0].id.toLowerCase();
    if(id == "home") {
        home();
    } else if(id == "billing_cycle") {
        clearElement('#mainContainer');
        simpleRequest("/api/billing_cycle", "booked money from:<br/>");
    } else if(id == "#3") {
        archive_byrts();
    } else if(id == "credits") {
        clearElement('#mainContainer');
        imprint();
    } else if(id == "zip_analysis") {
        clearElement('#mainContainer');
        simpleRequestHtml("/api/zip_analysis/?country=DE&returnAs=html", "<h2>All ZIP codes</h2><p>All ZIP codes for DE with member count.</p>");
    } else if(id == "country_analysis") {
        clearElement('#mainContainer');
        simpleRequestTable("/api/country_analysis/", "<h2>All active Members in Countries</h2>", ['ISO-2', 'Country', 'Members']);
    } else {
        //dummy_content();
    }
}


$(".nav a").on("click", function(){
     $(".nav").find(".active").removeClass("active");
     $(this).parent().addClass("active");
     contentControl();
});


getPendingMails();