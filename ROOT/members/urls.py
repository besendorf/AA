from django.conf.urls import url

from . import views

app_name = 'members'

urlpatterns = [
    url(r'^import_members/', views.run_import_members, name='import_members'),
    url(r'^add_member/', views.add_member, name='add_member'),
    url(r'^search_form/', views.search_member_db, name='search_form'),
    url(r'^member_address_unknown/', views.member_address_unknown, name='memberAddressUnknown'),
    url(r'^member_email_unknown/', views.bounces_import, name='memberEmailUnknown'),
    url(r'^member_bulk_exit/', views.member_bulk_exit, name='memberBulkExit'),

    url(r'^statistics/monthly/', views.monthly_statistics, name='monthly_statistics'),
    url(r'^statistics/overview/', views.general_statistics, name='general_statistics'),
    url(r'^statistics/zip/', views.zip_statistics, name='zip_statistics'),

    url(r'^erfaabgleich/cashpoint_export/', views.cashpoint_export, name='cashpoint_export'),
    url(r'^erfaabgleich/vereinstisch_import/', views.vereinstisch_import, name='vereinstisch_import'),
    url(r'^erfaabgleich/vereinstisch_export/', views.vereinstisch_export, name='vereinstisch_export'),
    url(r'^erfaabgleich/vereinstisch_erfaliste/', views.vereinstisch_erfaliste, name='vereinstisch_erfaliste'),

    url(r'^openslides_export/', views.openslides_export, name='openslides_export'),
    url(r'^check_teilnahmeschein/', views.check_teilnahmeschein, name='check_teilnahmeschein'),

    url(r'^premiumadress_import/', views.premiumadress_import, name='premiumadress_import'),
    url(r'^show_transaction_log/$', views.show_transaction_log, name='show_transaction_log'),
    url(r'^show_transaction_log/(\d+)$', views.show_transaction_log, name='show_member_transaction_log'),
    url(r'^select_mail_reruns/$', views.select_mail_reruns, name='select_mail_reruns'),
    url(r'^do_mail_rerun/(?P<run_date>.+)$', views.do_mail_rerun, name='do_mail_rerun'),
    url(r'^anti_transaction/(\d+)$', views.anti_transaction, name='anti_transaction'),
    url(r'^datenschleuder_address_stickers_selector/$', views.datenschleuder_address_stickers_selector,
        name='datenschleuder_address_stickers_selector'),
    url(r'^ga_invitations/$', views.ga_invitations_pa_selector,
        name='ga_invitations_pa_selector'),
    url(r'^change_membership_type/(\d+)$', views.change_membership_type, name='change_membership_type'),
]
