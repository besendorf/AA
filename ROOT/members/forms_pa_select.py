from django import forms
from members.models import PremiumAddressLabel


class AddressLabelForm(forms.Form):
    pa_label = forms.ModelChoiceField(queryset=PremiumAddressLabel.objects.all(), empty_label='None',
                                      label='PremiumAddress-Label', required=False)
